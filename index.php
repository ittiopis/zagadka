<?php

$size = 65536;

//print_r($size); die();
$a = [];
for ($key = 0, $i=0, $maxKey = ($size - 1) * $size; $key <= $maxKey; $key += $size, $i++) {
    $a[$i] = $key;
}
$startTime = microtime(true);


$array = array();
foreach ($a as $i=>$key) {
    $array[$key] = null;
}

$endTime = microtime(true);

printf("Inserted %d elements in %.2f seconds\n", $size, $endTime - $startTime);
